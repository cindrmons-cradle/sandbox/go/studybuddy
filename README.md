# Studybuddy

> A Go CLI Project made from [this](https://www.youtube.com/watch?v=so3VZwdWcBg) tutorial.

Learn a new language with the Studybuddy CLI at your side.

## How to install

```sh
$ go get install gitlab.com/cindrmons-cradle/sandbox/go/studybuddy
```
