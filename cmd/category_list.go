/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
)

func displayAllCategories() {
	categories := data.GetCategories()

	log.Println("All Available Categories: ")
	for _, item := range categories {
		log.Printf(item.Category)
	}
}

// categoryListCmd represents the categoryList command
var categoryListCmd = &cobra.Command{
	Use:   "list",
	Short: "lists all categories",
	Long:  `Lists out all available categories from the database.`,
	Run: func(cmd *cobra.Command, args []string) {
		displayAllCategories()
	},
}

func init() {
	categoryCmd.AddCommand(categoryListCmd)
}
