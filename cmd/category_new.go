/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/structs"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/util"
)

func createNewCategory() {

	// user input category
	categoryPromptContent := structs.PromptContent{
		ErrorMsg: "Please provide a word",
		Label:    "Enter a new category:  ",
	}
	categoryInput := util.PromptGetInput(categoryPromptContent)

	data.InsertCategory(categoryInput)
}

// categoryNewCmd represents the categoryNew command
var categoryNewCmd = &cobra.Command{
	Use:   "new",
	Short: "Creates a new category",
	Long:  `Creates a new note category`,
	Run: func(cmd *cobra.Command, args []string) {
		createNewCategory()
	},
}

func init() {
	categoryCmd.AddCommand(categoryNewCmd)
}
