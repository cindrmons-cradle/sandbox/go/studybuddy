/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/structs"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/util"
)

func createNewNote() {

	// set available categories
	rawCategories := data.GetCategories()
	categories := []string{}

	for _, item := range rawCategories {
		categories = append(categories, item.Category)
	}

	// user input word
	wordPromptContent := structs.PromptContent{
		ErrorMsg: "Please provide a word",
		Label:    "What word would you like to make a note of?  ",
	}
	wordInput := util.PromptGetInput(wordPromptContent)

	// user input definition
	definitionPromptContent := structs.PromptContent{
		ErrorMsg: "Please provide a definition",
		Label:    fmt.Sprintf("What is the definition of %s?  ", wordInput),
	}
	definitionInput := util.PromptGetInput(definitionPromptContent)

	// user input category
	categoryPromptContent := structs.PromptContent{
		ErrorMsg: "Please provide a category",
		Label:    fmt.Sprintf("What category does %s belong to?  ", wordInput),
	}
	categoryInput := util.PromptGetSelect(categoryPromptContent, categories, func(res string) {
		data.InsertCategory(res)
	})

	data.InsertNote(wordInput, definitionInput, categoryInput)
}

// newCmd represents the new command
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Add a new note.",
	Long:  `Command for adding a new note.`,
	Run: func(cmd *cobra.Command, args []string) {
		createNewNote()
	},
}

func init() {
	noteCmd.AddCommand(newCmd)
}
