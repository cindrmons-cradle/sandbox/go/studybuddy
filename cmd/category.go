/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"
)

// categoryCmd represents the category command
var categoryCmd = &cobra.Command{
	Use:   "category",
	Short: "A classification of multiple notes.",
	Long:  `A classification of multiple notes.`,
}

func init() {
	rootCmd.AddCommand(categoryCmd)
}
