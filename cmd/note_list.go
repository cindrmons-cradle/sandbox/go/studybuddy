/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
)

func displayAllNotes() {
	notes := data.GetNotes()

	for _, item := range notes {
		log.Printf("%s (%s) -- %s", item.Word, item.Category, item.Definition)
	}
}

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "Lists all notes that you have currently added.",
	Long:  `Lists all notes that you have currently added.`,
	Run: func(cmd *cobra.Command, args []string) {
		displayAllNotes()
	},
}

func init() {
	noteCmd.AddCommand(listCmd)
}
