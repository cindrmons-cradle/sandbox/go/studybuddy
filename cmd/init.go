/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialise Studybuddy database",
	Long:  `Initialise a Studybuddy database`,
	Run: func(cmd *cobra.Command, args []string) {
		data.InitialiseTables()
		data.InsertCategory("animal")
		data.InsertCategory("food")
		data.InsertCategory("person")
		data.InsertCategory("object")
		data.InsertCategory("code")
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
