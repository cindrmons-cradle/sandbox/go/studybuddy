package data

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/structs"
)

var dbct *sql.DB

func OpenDB() error {

	var dbErr error

	dbct, dbErr = sql.Open("sqlite3", "./sqlite-database.db")

	if dbErr != nil {
		return dbErr
	}

	return dbct.Ping()
}

func InitialiseTables() {
	createCategoriesTableQ := `CREATE TABLE IF NOT EXISTS categories (
		"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"category" TEXT
	);`
	createNotesTableQ := `CREATE TABLE IF NOT EXISTS notes (
		"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"word" TEXT,
		"definition" TEXT,
		"category_id" INT NOT NULL,
		FOREIGN KEY(category_id) REFERENCES categories(id)
	);`

	stmt, stmtErr := dbct.Prepare(createCategoriesTableQ)
	if stmtErr != nil {
		log.Fatal(stmtErr.Error())
	}
	stmt.Exec()
	log.Println("`categories` Table Created!")

	stmt, stmtErr = dbct.Prepare(createNotesTableQ)
	if stmtErr != nil {
		log.Fatal(stmtErr.Error())
	}
	stmt.Exec()
	log.Println("`notes` Table Created!")
}

func GetCategories() []structs.Category {
	categories := []structs.Category{}
	row, sqlErr := dbct.Query("SELECT * FROM categories")
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	for row.Next() {
		var currCategory structs.Category
		row.Scan(&currCategory.Id, &currCategory.Category)
		categories = append(categories, currCategory)
	}

	return categories
}

func GetNotes() []structs.Note {
	notes := []structs.Note{}
	row, sqlErr := dbct.Query(`SELECT notes.id, notes.word, notes.definition, categories.category
	FROM notes
	INNER JOIN categories
	WHERE notes.category_id = categories.id
	ORDER BY word;`)
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	for row.Next() {
		var currNote structs.Note
		row.Scan(&currNote.Id, &currNote.Word, &currNote.Definition, &currNote.Category)
		notes = append(notes, currNote)
	}

	return notes
}

func InsertCategory(category string) {
	q := `INSERT INTO categories (category) VALUES (?);`

	stmt, sqlErr := dbct.Prepare(q)
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	_, sqlErr = stmt.Exec(category)
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	log.Printf("Category %s successfully added!\n", category)
}

func InsertNote(word string, definition string, category string) {
	insertNoteQuery := `INSERT INTO notes (word, definition, category_id) VALUES (?, ?, ?)`
	selectedCategory := structs.Category{}

	stmt, sqlErr := dbct.Prepare(insertNoteQuery)
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	// search for category in `categories` table
	for _, item := range GetCategories() {
		if item.Category == category {
			selectedCategory = item
			break
		}
	}

	// crash (for now) if category does not exist
	if (selectedCategory == structs.Category{}) {
		log.Fatalln("Category does not exist.")
	}

	_, sqlErr = stmt.Exec(word, definition, selectedCategory.Id)
	if sqlErr != nil {
		log.Fatalln(sqlErr)
	}

	log.Println("Study Note Inserted!")
}
