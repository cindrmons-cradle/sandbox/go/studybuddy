package util

import (
	"errors"
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/structs"
)

func PromptGetInput(prco structs.PromptContent) string {
	validate := func(input string) error {
		if len(input) <= 0 {
			return errors.New(prco.ErrorMsg)
		}

		return nil
	}

	templates := &promptui.PromptTemplates{
		Prompt:  "{{ . }}",
		Valid:   "{{ . | green }}",
		Invalid: "{{ . | red }}",
		Success: "{{ . | bold }}",
	}

	prompt := promptui.Prompt{
		Label:     prco.Label,
		Templates: templates,
		Validate:  validate,
	}

	res, prErr := prompt.Run()
	if prErr != nil {
		fmt.Printf("Input Prompt Failed...\n\t%v", prErr)
		os.Exit(1)
	}

	return res
}

func PromptGetSelect(prco structs.PromptContent, items []string, addMethod func(res string)) string {
	idx := -1

	var res string
	var prErr error

	for idx < 0 {
		prompt := promptui.SelectWithAdd{
			Label:    prco.Label,
			Items:    items,
			AddLabel: "other",
		}

		idx, res, prErr = prompt.Run()

		// add a new item method runs here
		if idx == -1 {
			addMethod(res)
			items = append(items, res)
		}
	}

	if prErr != nil {
		fmt.Printf("Prompt Failed...\n\t%v", prErr)
		os.Exit(1)
	}

	return res
}
