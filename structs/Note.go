package structs

type Note struct {
	Id         int
	Word       string
	Definition string
	Category   string
}
