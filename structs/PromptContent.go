package structs

type PromptContent struct {
	ErrorMsg string
	Label    string
}
