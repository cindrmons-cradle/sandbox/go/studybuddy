/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/cmd"
	"gitlab.com/cindrmons-cradle/sandbox/go/studybuddy/data"
)

func main() {
	data.OpenDB()
	cmd.Execute()
}
